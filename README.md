# BloggingAPI

## Setup project
```
docker-compose run web pip install --user -r requirements.txt
docker-compose up -d mysql
docker-compose run web python manage.py migrate
```


## Run project
```
docker-compose up
```