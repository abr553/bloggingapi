from rest_framework.routers import DefaultRouter
from post import views
from django.urls import path

URL_USER = 'posts'
URL_USER_LIST = URL_USER + '-list'
URL_USER_DETAIL = URL_USER + '-detail'

router = DefaultRouter()
router.register('posts', views.PostViewSet, basename='posts')

urlpatterns = router.urls

