from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.viewsets import ModelViewSet
from post.models import Post
from post.serializers import PostSerializer
from rest_framework import permissions, filters
from app.permissions import IsOwner
from django.utils.timezone import make_aware
from datetime import datetime

class PostViewSet(ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication, TokenAuthentication]
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['title', 'releaseDate']
    ordering = ['-releaseDate']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.permissions_classes = []

    def get_queryset(self):
        self.queryset = self.queryset.filter(releaseDate__lt=make_aware(datetime.now()))
        return super().get_queryset()

    def get_permissions(self):
        self.permissions_classes = [permissions.IsAuthenticated, IsOwner]
        if self.action == 'create':
            self.permissions_classes = [permissions.IsAuthenticated]
        if self.action in ['list', 'retrieve']:
            self.permissions_classes = [permissions.AllowAny]
        return super().get_permissions()