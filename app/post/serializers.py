from rest_framework import serializers
from post.models import Post
from user.models import User

class PostSerializer(serializers.ModelSerializer):
    owner = serializers.PrimaryKeyRelatedField(many=False, queryset=User.objects.all())

    class Meta:
        model = Post
        fields = ('id', 'title', 'content', 'urlImage', 'releaseDate', 'owner')
        read_only_fields = ('id',)
