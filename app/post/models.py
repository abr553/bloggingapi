from django.db import models
from user.models import User

class Post(models.Model):
    title = models.CharField(max_length=128)
    content = models.TextField()
    urlImage = models.URLField(max_length=500)
    releaseDate = models.DateTimeField()

    owner = models.ForeignKey(User, on_delete=models.CASCADE)