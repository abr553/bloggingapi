from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.viewsets import ModelViewSet
from user.models import User
from user.serializers import UserSerializer
from rest_framework import permissions
from app.permissions import IsOwner

class UserViewSet(ModelViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.permissions_classes = []

    def get_permissions(self):
        self.permissions_classes = [permissions.IsAuthenticated, IsOwner]
        if self.action in ['create', 'retrieve']:
            self.permissions_classes = [permissions.AllowAny]
        return [permission() for permission in self.permissions_classes]