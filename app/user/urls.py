from rest_framework.routers import DefaultRouter
from user import views
from django.urls import path

URL_USER = 'users'
URL_USER_LIST = URL_USER + '-list'
URL_USER_DETAIL = URL_USER + '-detail'

router = DefaultRouter()
router.register('users', views.UserViewSet, basename='users')

urlpatterns = router.urls

