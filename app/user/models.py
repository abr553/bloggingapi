from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password, **extra_fields):
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password, **extra_fields):
        return self._create_user(username, password, **extra_fields)

    def create(self, username, password, **extra_fields):
        from user.models import User
        prev_user = self.filter(username=username,is_active=False).first()
        if not prev_user is None:
            prev_user.is_active = True
            prev_user.set_password(password)
            prev_user.save(using=self._db)
            return prev_user
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(username, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(blank=False, null=False)
    username = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=32)
    lastName = models.CharField(max_length=64)
    is_superuser = models.BooleanField(blank=True, default=False)
    is_active = models.BooleanField(default=True)

    is_staff = True

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['password']
