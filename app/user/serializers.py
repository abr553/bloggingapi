from rest_framework import serializers
from user.models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'is_superuser', 'email', 'username', 'password', 'name', 'lastName')
        read_only_fields = ('id', 'is_superuser')
        extra_kwargs = {
            'password': {'write_only': True}
        }
