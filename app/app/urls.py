from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
# from rest_framework.authtoken import views
from app.views import CustomAuthToken

urlpatterns = [
    path('', include('user.urls')), 
    path('', include('post.urls')),
    path('auth/', CustomAuthToken.as_view()),
    path('admin/', admin.site.urls)
]
